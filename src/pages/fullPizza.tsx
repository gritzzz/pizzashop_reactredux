import axios from 'axios';
import React, { FC, useEffect, useState } from 'react'
import { useParams } from 'react-router';

const FullPizza: FC = () => {
    const { id } = useParams();
    const [pizza, setPizza] = useState<{
        imageUrl: string;
        name: string;
        price: number;
        rating: number;
    }>();

    useEffect(() => {
        async function fetchPizza() {
            try {
                const { data } = await axios.get('https://6595b27804335332df832a49.mockapi.io/api/items/' + id);
                setPizza(data);
            } catch (error) {
                console.log(error);
            }
        }
        fetchPizza();
    }, []);

    if (!pizza) {
        return 'Загрузка...';
    }
    return (
        <div className='container'>
            <img src={pizza.imageUrl} alt='' />
            <h2>{pizza.name}</h2>
            <p>Рейтинг: {pizza.rating}</p>
            <h4>Цена: {pizza.price} руб.</h4>
        </div>
    )
}

export default FullPizza;
