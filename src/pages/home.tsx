import { FC, useCallback, useEffect, useRef } from "react";
import Sort, { sortItems } from "../components/sort";
import Skeleton from "../components/pizzaBlock/skeleton";
import PizzaBlock from "../components/pizzaBlock";
import Pagination from "../pagination";
import { useSelector } from "react-redux";
import qs from "qs";
import { useNavigate } from "react-router-dom";
import Categories from "../components/categories";
import { useAppDispatch } from "../redux/store";
import { selectFilter } from "../redux/slices/filter/selectors";
import { selectPizza } from "../redux/slices/pizza/selectors";
import { setCategoryId, setCurrentPage, setFilters } from "../redux/slices/filter/slice";
import { fetchPizzas } from "../redux/slices/pizza/slice";
import { SearchPizzaParams } from "../redux/slices/pizza/types";

const Home: FC = () => {
    const { categoryId, sort, currentPage, searchValue } = useSelector(selectFilter);
    const { items, status } = useSelector(selectPizza);

    const sortType = sort.sort;
    const dispatch = useAppDispatch();
    const navigate = useNavigate();

    const isSearch = useRef(false);
    const isMounted = useRef(false);

    const pizzas = items.map((obj: any) => (<PizzaBlock {...obj} key={obj.id}  />));
    const skeletons = [...new Array(6)].map((_, i) => <Skeleton key={i} />);

    const onChangePage = useCallback((number: number) => {
        dispatch(setCurrentPage(number));
    },[])
    const getPizzas = async () => {
        const sortBy = sort.sort.replace('-', '');
    const order = sort.sort.includes('-') ? 'asc' : 'desc';
    const category = categoryId > 0 ? String(categoryId) : '';
    const search = searchValue;

        dispatch(
            fetchPizzas({
            category,
            sortBy,
            order,
            search,
            currentPage: String(currentPage),
        }));

        window.scrollTo(0, 0);
    };

    //запрос пицц с бэка если был первый рендер
    useEffect(() => {
        getPizzas();
    }, [categoryId, sort.sort, searchValue, currentPage]);

    // если рендер уже был и параметры изменили, то поменять url
    useEffect(() => {
        if (isMounted.current) {
            const querryString = qs.stringify({
                sort: sort.sort,
                categoryId,
                currentPage,
            });
            navigate(`?${querryString}`);
        }
        if (window.location.search) {
            dispatch(fetchPizzas({} as SearchPizzaParams));
        }
        isMounted.current = true;
    }, [categoryId, sort.sort, searchValue, currentPage]);

    // если был первый рендер проверяем url строку и пишем в redux
    useEffect(() => {
        if (window.location.search) {
            const params = qs.parse(window.location.search.substring(1)) as unknown as SearchPizzaParams ;
            const sort = sortItems.find(obj => obj.sort === params.sortBy);

            dispatch(setFilters({
                searchValue: params.search,
                categoryId: Number(params.category),
                currentPage: Number(params.currentPage),
                sort: sort || sortItems[0],

            }));
            isSearch.current = true;
        }
    }, []);

    return (
        <div className="container">
            <div className="content__top">
                <Categories value={categoryId} onClickCategory={(i:number) => dispatch(setCategoryId(i))} />
                <Sort />
            </div>
            <h2 className="content__title">Все пиццы</h2>
            {
                status === 'error' ? <div className="content__error-info">
                    <h2>Произошла ошибка 😕</h2>
                    <p>
                        Не удалось получить пиццы<br/> Попробуйте через пару недель
                    </p>
                </div> : <div className="content__items">
                    {
                        status === 'loading'
                            ? skeletons
                            : pizzas
                    }
                </div>
            }
            <Pagination onChangePage={onChangePage} currentPage={currentPage} />
        </div>
    )
}

export default Home;