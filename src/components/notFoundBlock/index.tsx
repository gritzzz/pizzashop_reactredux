import { FC } from 'react';
import styles from './notFoundBlock.module.scss'

const NotFoundBlock: FC = () => {
    return (
        <div className={styles.root}>
            <h1>
                <span>😒</span>
                <br />404
            </h1>
            <p className={styles.description}>Такой страницы не имею</p>
        </div>
    );
};

export default NotFoundBlock;