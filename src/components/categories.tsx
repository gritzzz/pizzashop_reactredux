import React, { FC } from 'react'

type CategoriesProps = {
  value: number;
  onClickCategory: (index: number) => void;
};

const Categories: FC<CategoriesProps> = ({ value, onClickCategory }) => {

  const categories = [
    'Все',
    'Мясные',
    'Вегетарианские',
    'Гриль',
    'Острые',
    'Закрытые',
  ];

  return (
    <div className="categories">
      <ul>
        {
          categories.map((v, index) => {
            return (
              <li key={index} onClick={() => onClickCategory(index)} className={value === index ? 'active' : ''}>{v}</li>
            )
          })
        }

      </ul>
    </div>
  );
}

export default Categories;