import React, { FC } from "react"
import ContentLoader from "react-content-loader"

const Skeleton: FC = () => (
  <ContentLoader 
  className="pizza-block"
    speed={2}
    width={280}
    height={500}
    viewBox="0 0 280 500"
    backgroundColor="#f3f3f3"
    foregroundColor="#ecebeb"
    
  >
    <circle cx="140" cy="128" r="128" /> 
    <rect x="0" y="263" rx="15" ry="15" width="280" height="38" /> 
    <rect x="0" y="310" rx="15" ry="15" width="280" height="88" /> 
    <rect x="0" y="420" rx="10" ry="10" width="105" height="45" /> 
    <rect x="125" y="420" rx="20" ry="20" width="152" height="45" />
  </ContentLoader>
)

export default Skeleton;